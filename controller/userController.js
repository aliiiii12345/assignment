const response = require('../response/response')
const userModel = require('../model/user')
const bcrypt = require('bcrypt');
const JWT = require('../jwt/generateToken');
const JWTverify = require('../jwt/auth')
const utils = require('../utils/userUtils')
const userModule = require('../module/userModule')

exports.signUp = async ( req , res ) => {
        try{
            let { name , email , password } = req.body
    
            let dataToSave = {
                name:name,
                email:email,
                password:password
            }
    
            // set user password to hashed password
            dataToSave.password = await utils.convertHash( password );
    
            // find for email is exist already or not
            let isEmailExists = await userModule.findByEmail(email)
    
            if (isEmailExists) {
                response.sendError({ errorCode: "emailAlreadyExists", error: "" }, res, 400);
                return;
            }
    
            // save if user already not exist
            let results = await userModule.createUser( dataToSave )
    
            if( !results ) throw {message:'Something went wrong' , status:400}
    
            // updating token
            let accessToken = await JWT.generateJwt({profileId: results._id ,expiresIn: '30d', profileType:'user' });
            let updateToken = await userModule.updateUser( results._id , accessToken )
    
            response.sendSuccess({ result: updateToken, successCode: 'userAdded' }, res, 200);
    
        }catch( error ){
            response.sendError({ errorCode: "serverErrordd", error: error.message }, res, 400);
        }
    
}


exports.login = async ( req , res ) => {
    try{
        let { email , password } = req.body

        // find with email
        let matchCred = await userModule.findByEmail( email )

        // comparing hash password to given password
        const validPassword = await utils.comparePassword( password , matchCred.password )

        if ( matchCred.length < 1 ) throw { message:'Invalid email ID' , status:400 }

        if ( !validPassword ) throw { message:'Wrong credential' , status:400 }

        let accessToken = await JWT.generateJwt({profileId: matchCred._id , expiresIn: '30d', profileType:'user' })

        // updating token if credential match
        var updateToken = await userModule.updateUser( matchCred._id , accessToken )

        response.sendSuccess({ result: updateToken, successCode: 'userLogin' }, res, 200);

    }catch( error ) {
        response.sendError({ errorCode: "serverError", error: error.message }, res, error.status);
    }
}


exports.logout = async ( req , res ) => {
    try{
        // we are verifying by token as like session, so after logout we make token to null
        let logoutUser = await userModule.updateUser( req.data.profileId , null )

        if ( !logoutUser ) throw { message:'Something went wrong' , status:400 }

        response.sendSuccess({ result: true, successCode: 'userLogout' }, res, 200);

    }catch( error ){
        response.sendError({ errorCode: "serverError", error: error.message }, res, error.status);
    }
}