## prerequisite
- node(v12 or above)

## setup
- go to the root directory
- run ```npm i```

## how to run
- for dev environment
    - ```npm run start```
    - ```nodemon index.js```

## Postman collection url
   
   - ```  https://www.getpostman.com/collections/b78136ff11c11aaa24e1  ```

