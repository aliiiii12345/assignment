const response = require('../response/response')
const userModel = require('../model/user')


exports.findByEmail = async (email) => {
    try{
        let findByEmail = await userModel.findOne({ email:email }).exec();
        return findByEmail
    }catch(error){
        response.sendError({ errorCode: "serverError", error: error.message }, res, 400);
    }
}


exports.createUser = async ( dataToSave ) => {
        let saveUser = new userModel( dataToSave );
        let result = await saveUser.save( dataToSave );
        return result
}


exports.updateUser = async ( id , token ) => {
        const userUpdate = await userModel.findByIdAndUpdate(id, { accessToken:token }, { new: true });
        return userUpdate
}


exports.findByEmail = async ( email ) => {
        const findbyemail = await userModel.findOne({
            email:email
        }).lean()
        return findbyemail
}