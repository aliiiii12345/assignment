const jwt = require('jsonwebtoken');
const response = require('../response/response');
require('dotenv').config()
const secretKey = process.env.JWTSECRET
function generateJwt(payLoad) {
    return new Promise((resolve, reject) => {
        jwt.sign(payLoad, secretKey, { expiresIn: payLoad.expiresIn }, (error, token) => {
            if (error) {
                console.log("TCL: generateJwt -> error", error)
                response.sendCustomError({ error: error, errorCode: 'apiStatus' })
            }
            else {
                resolve(token)
            }
        })
    })
}

module.exports = { generateJwt }