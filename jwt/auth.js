const jwt = require("jsonwebtoken");
const response = require("../response/response");
require('dotenv').config()
const secretkey = process.env.JWTSECRET
const userModel = require("../model/user");
const mongoose = require("mongoose");

const verifyUserToken = (req, res, next) => {
  let token = req.headers["accesstoken"];
  if (!token)
    return response.sendError({ error: false, errorCode: "auth" }, res, 401);
  jwt.verify(token, secretkey, async function (error, decoded) {
    if (error) {
      console.log("------------>Token ERROR", error);
      response.sendError({ error, errorCode: "serverError" }, res, 401);
    } else {
      if ( decoded.profileType === "user" ) {
        req.data = {
          profileId: decoded.profileId,
          accessToken: token,
          profileType: decoded.profileType,
        };
        // check on every request user is active or not
        let userData = await userModel.findOne({
          _id: mongoose.Types.ObjectId(req.data.profileId),
        });

        if (!userData) {
          console.log("User not found");
          response.sendError(
            { error: "Not found", errorCode: "auth" },
            res,
            401
          );
          return;
        }
        next();
      } else {
        console.log("Invalid user type");
        response.sendError({ error: false, errorCode: "auth" }, res, 401);
      }
    }
  });
}

module.exports = {
  verifyUserToken
};

