const axios = require('axios');

let fetchNews = async function ( query ) {
  let response = await axios.get(
    `https://newsapi.org/v2/everything?q=Apple&from=2022-01-10&sortBy=popularity&apiKey=${query.apiKey}`,
    {
      params: {
        apiKey: "5839df0629e14ac29807cdd7009bbd3a"
    },
    }
  );
  return response;
};

module.exports = {
  fetchNews: fetchNews,
};
