let chai = require('chai')
let chaiHttp = require('chai-http')
let server = require('../index')

// Assertion style
chai.should()

chai.use(chaiHttp)

describe('USER api' , () => {
     describe("POST /user/signUp" , () => {
         it("it is working on sign up" , ( done ) => {
             const data = {
                 name:"ali mo",
                 email:"alxi@gmail.com",
                 password:'qwertyuiop'
             }

             chai.request(server)
             .post("/user/signUp")
             .send(data)
             .end(( err , response ) => {
                response.should.have.status(200);
                done()
             })
         })
     })



})