const weatherData = require('./src/app/weather')
const news = require('./src/app/news')
require('dotenv').config()

const express = require('express')
const app = express()
const DB = require('./connection/connection')
app.use(express.json())
const port = process.env.PORT != null ? process.env.PORT : 3000

app.use('/user', require('./routes/routes'))
app.get('/weather', async function (req, res) {
  let responseData = await weatherData.getWeather( req.query )
  res.status(responseData.status).send(responseData.data)
})

app.get('/news', async function (req, res) {
  let responseData = await news.fetchNews(req.query)
  res.status(responseData.status).send(responseData.data)
})




app.listen(port, () => {
  console.log('The server is running at port', port)
})


