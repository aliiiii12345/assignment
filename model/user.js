const mongoose = require("mongoose");
const Types = mongoose.Schema.Types;

// FAQ Schema
const userSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required:true
        },
        
        email: {
            type: String,
            required:true
        },

        accessToken: {
            type: String
        },

        password:{
            type:String,
            required:true
        }
    },
    { timestamps: true }
);

userSchema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});

const user = mongoose.model("user", userSchema);
module.exports = user;
