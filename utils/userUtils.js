const bcrypt = require('bcrypt');
const response = require('../response/response')
const JWT = require('../jwt/generateToken');
const JWTverify = require('../jwt/auth')



exports.convertHash = async ( password ) => {
    try{
        let salt = await bcrypt.genSalt(10)
        return await bcrypt.hash( password , salt )
    }catch( error ){

    }
}


exports.comparePassword = async ( password1 , password2 ) => {
    try{
        let compare = await bcrypt.compare(password1 , password2);
        return compare
    }catch( error ){

    }
}