const JOI = require('joi')

exports.signUp = async ( req , res , next ) => {
    try{
        let schema = JOI.object({
            name:JOI.string().min(3).max(10).required(),
            email:JOI.string().min(3).max(50).required(),
            password:JOI.string().min(7).max(20).required()
        })

        let result = schema.validate( req.body )

        if ( result.error ) {
            res.status(400).send({error:result.error.details[0].message})
            return
        }
        next()
    }catch(error){
        res.status(400).send(error)
    }
}


exports.login = async ( req , res , next ) => {
    try{
        let schema = JOI.object({
            email:JOI.string().min(3).max(50).required(),
            password:JOI.string().min(7).max(20).required()
        })

        let result = schema.validate( req.body )

        if ( result.error ) {
            res.status(400).send({error:result.error.details[0].message})
            return
        }
        next()
    }catch(error){
        res.status(400).send(error)
    }
}