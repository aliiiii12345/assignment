const express  = require('express');
const router = express.Router();
const response = require('../response/response')
var userModel = require('../model/user')
const bcrypt = require('bcrypt');
const JWT = require('../jwt/generateToken');
const JWTverify = require('../jwt/auth')
const userController = require('../controller/userController')
const joiValidation = require('../joi/joi')




// ROUTES
router.get('/', async function (req, res) {
    res.send({a:req.body})
});


//   SIGNUP
router.post('/signUp', joiValidation.signUp , userController.signUp)


//    LOGIN
router.post('/login', joiValidation.login , userController.login)

//    LOGOUT 
router.post('/logout', JWTverify.verifyUserToken , userController.logout)

module.exports = router;